/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.demo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTests {

    String existingUser = "Anna";
    String nonexistentUser = "Рон";
    long existingCompany = 0;
    long existingCompanyTwo = 72;
    long nonexistentCompany = 166L;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void paramsExistingCompanyAndExistingUserShouldReturnTailoredMessage() throws Exception {

        this.mockMvc.perform(get("/company/" + existingCompany + "/users").param("name", existingUser))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.company").value(existingCompany))
                .andExpect(jsonPath("$.name").value(existingUser));
    }

    @Test
    public void paramsExistingCompanyAndNonexistentUserShouldReturnNotFound() throws Exception {

        this.mockMvc.perform(get("/company/" + existingCompany + "/users").param("name", nonexistentUser))
                .andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void paramsNonexistentCompanyAndNonexistentUserShouldReturnNotFound() throws Exception {

        this.mockMvc.perform(get("/company/" + nonexistentCompany + "/users").param("name", nonexistentUser))
                .andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void paramsOnlyExistingCompanyShouldReturnBadRequest() throws Exception {

        this.mockMvc.perform(get("/company/" + existingCompany + "/users"))
                .andDo(print()).andExpect(status().isBadRequest());
    }

    @Test
    public void paramsOnlyExistingUserShouldReturnNotFound() throws Exception {

        this.mockMvc.perform(get("/company//users").param("name", existingUser))
                .andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void noParamsShouldReturnNotFound() throws Exception {

        this.mockMvc.perform(get("/company//users"))
                .andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void paramsExistingCompanyZeroAndExistingUserShouldReturnTailoredMessage() throws Exception {

        this.mockMvc.perform(get("/company/0/users").param("name", existingUser))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.company").value(existingCompany))
                .andExpect(jsonPath("$.name").value(existingUser));
    }

    @Test
    public void paramsExistingCompanyMinusOneAndExistingUserShouldReturnNotFound() throws Exception {

        this.mockMvc.perform(get("/company/-1/users").param("name", existingUser))
                .andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void paramsExistingCompanyNotNumberAndExistingUserShouldReturnNotFound() throws Exception {

        this.mockMvc.perform(get("/company/string/users").param("name", existingUser))
                .andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void paramsExistingCompanyBigIdAndExistingUserShouldReturnOk() throws Exception {
        
        long bigId = Long.MAX_VALUE;
        this.mockMvc.perform(get("/company/" + bigId + "/users").param("name", existingUser))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void paramsExistingCompanyAndExistingUsersShouldReturnTailoredMessage() throws Exception {
 
        this.mockMvc.perform(get("/company/" + existingCompany + "/users").param("name", existingUser, existingUser))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.company").value(existingCompany))
                .andExpect(jsonPath("$.name").value(existingUser + "," + existingUser));
    }

    @Test
    public void paramsCompanyAndUserFromAnotherCompanyShouldReturnForbidden() throws Exception {
        long company = 88L;
        String userFromAnotherCompany = "Alex";
        this.mockMvc.perform(get("/company/" + company + "/users").param("name", userFromAnotherCompany))
                .andDo(print()).andExpect(status().isForbidden());
    }

    // How to implement Time out test, Ijection tests?
}
