package com.example.demo;

public class User {

    private final long company;
    private final String name;

    public User(String name, long company) {
        this.company = company;
        this.name = name;
    }

    public long getCompany() {
        return company;
    }

    public String getName() {
        return name;
    }
}
