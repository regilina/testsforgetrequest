package com.example.demo;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @RequestMapping("/company/{company}/users")
    public User greeting(@PathVariable(name = "company") String company, @RequestParam(value="name") String name) {
        return new User(name, Long.parseLong(company));
    }
}
